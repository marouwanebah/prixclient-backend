CREATE DATABASE PC; 

CREATE TABLE IF NOT EXISTS User (
    id  integer NOT NULL AUTO_INCREMENT,
    nom varchar(15),
    prenom varchar(30),
    telephone integer,
	email varchar(30),
    PRIMARY KEY (id)
)
ENGINE=INNODB;