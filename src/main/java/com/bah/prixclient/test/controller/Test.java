package com.bah.prixclient.test.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bah.prixclient.database.repository.UserRepository;
import com.bah.prixclient.referentiel.model.IDTOEntity;

/**
 * Class test pour l'api resdt. 
 * @author Marwane
 *
 */
@RestController
@RequestMapping("api")
public class Test {

	@Autowired
	private UserRepository repoUsers;

	@SuppressWarnings("rawtypes")
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/test")
	public List<IDTOEntity> getTest() {
		return (this.repoUsers.findAll().stream().map(x -> x.convertToDto()).collect(Collectors.toList()));
	}
}
