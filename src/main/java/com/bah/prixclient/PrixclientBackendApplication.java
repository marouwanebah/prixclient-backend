package com.bah.prixclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrixclientBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrixclientBackendApplication.class, args);
	}

}
