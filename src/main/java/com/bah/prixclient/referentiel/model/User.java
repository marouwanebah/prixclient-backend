package com.bah.prixclient.referentiel.model;

import com.bah.prixclient.database.entity.UserEntity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class User implements IDTOEntity<UserEntity> {

	/**
	 * L'indentifiant de l'utilisateur.
	 */
	private Integer id;

	/**
	 * Le nom de l'utilisateur.
	 */
	private String nom;

	/**
	 * Le prenom de l'utilisateur.
	 */
	private String prenom;

	/**
	 * Le numero telephone de l'utilisateur.
	 */
	private Integer telephone;

	/**
	 * L'email de l'utilisateur.
	 */
	private String email;

	/**
	 * Le construteur.
	 * 
	 * @param userEntity
	 */
	public User(final UserEntity userEntity) {
		this.id = userEntity.getId();
		this.nom = userEntity.getNom();
		this.prenom = userEntity.getPrenom();
		this.telephone = userEntity.getTelephone();
		this.email = userEntity.getEmail();
	}
	
	@Override
	public UserEntity convertToEntity() {
		return new UserEntity(this.id, this.nom, this.prenom, this.telephone, this.email);
	}

}
