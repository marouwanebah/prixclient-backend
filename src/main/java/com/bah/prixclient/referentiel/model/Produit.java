package com.bah.prixclient.referentiel.model;

import com.bah.prixclient.database.entity.ProduitEntity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Produit implements IDTOEntity<ProduitEntity> {

	/**
	 * L'indentifiant du produit.
	 */
	private Integer id;

	/**
	 * Le nom du produit.
	 */
	private String nom;

	/**
	 * Le prix du produit.
	 */
	private Integer prix;

	/**
	 * Catégorie du produit.
	 */
	private String categorie;

	/**
	 * Le contructeur.
	 * 
	 * @param produitEntity
	 */
	public Produit(final ProduitEntity produitEntity) {
		this.id = produitEntity.getId();
		this.nom = produitEntity.getNom();
		this.prix = produitEntity.getPrix();
		this.categorie = produitEntity.getCategorie();

	}

	@Override
	public ProduitEntity convertToEntity() {
		// TODO Auto-generated method stub
		return null;
	}
}
