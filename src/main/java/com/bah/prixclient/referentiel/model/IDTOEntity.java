package com.bah.prixclient.referentiel.model;

/**
 * Interface pour la convertion de DTO en Entity.
 * 
 * @author Marwane
 *
 * @param <T>
 */
public interface IDTOEntity<T> {
	
	/**
	 * Methode de conversion d'un entity<T>
	 * @return L'oject T correspondant a l'object DTO
	 */
	T convertToEntity();

}
