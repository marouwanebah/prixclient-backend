package com.bah.prixclient.referentiel.controller;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.bah.prixclient.database.entity.IEntity;
import com.bah.prixclient.referentiel.model.IDTOEntity;

/**
 * Le Crud pour les controllers.
 * 
 * @author Marwane
 *
 * @param <T>
 * @param <Y>
 * @param <Z>
 */
public class CrudRestController<T extends IEntity<Z>, Y extends IDTOEntity<T>, Z extends Serializable> {

	protected JpaRepository<T, Z> repo;

	/**
	 * 
	 * @param repo
	 */
	public CrudRestController(JpaRepository<T, Z> repo) {
		this.repo = repo;
	}

	/**
	 * Retour la liste de tous les objet du repo.
	 * 
	 * @return Objects du repo.
	 */
	@SuppressWarnings("rawtypes")
	@GetMapping
	public ResponseEntity<List<IDTOEntity>> getAll() {
		return ResponseEntity.ok(this.repo.findAll().stream().map(x -> x.convertToDto()).collect(Collectors.toList()));
	}

	/**
	 * Retourne un objet du repo via son id.
	 * @param id l'identifiant de l'oject recherché 
	 * @return L'object correspondant
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<IDTOEntity<T>> getById(@PathVariable Z id) {
		@SuppressWarnings("unchecked")
		IDTOEntity<T> convertToDto = this.repo.findById(id).get().convertToDto();
		return ResponseEntity.ok(convertToDto);
	}
}
