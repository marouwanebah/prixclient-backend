package com.bah.prixclient.referentiel.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bah.prixclient.database.entity.ProduitEntity;
import com.bah.prixclient.database.repository.ProduitRepository;
import com.bah.prixclient.referentiel.model.Produit;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/produit")
public class ProduitController extends CrudRestController<ProduitEntity, Produit, Integer> {

	public ProduitController(ProduitRepository repo) {
		super(repo);
	}

}
