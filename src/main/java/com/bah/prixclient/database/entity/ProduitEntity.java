package com.bah.prixclient.database.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bah.prixclient.referentiel.model.IDTOEntity;
import com.bah.prixclient.referentiel.model.Produit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entity pour les produits.
 * 
 * @author Marwane
 *
 */
@Table(name = "TABLE_PRODUIT")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class ProduitEntity implements IEntity<Integer> {

	/**
	 * L'indentifiant du produit.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * Le nom du produit.
	 */
	@Column(name = "nom")
	private String nom;

	/**
	 * Le prix du produit.
	 */
	@Column(name = "prix")
	private Integer prix;

	/**
	 * Catégorie du produit.
	 */
	@Column(name = "categorie")
	private String categorie;

	@SuppressWarnings("rawtypes")
	@Override
	public IDTOEntity convertToDto() {
		return new Produit(this);
	}

}
