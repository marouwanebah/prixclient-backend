package com.bah.prixclient.database.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bah.prixclient.referentiel.model.IDTOEntity;
import com.bah.prixclient.referentiel.model.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entity pour les utilisateurs.
 * 
 * @author Marwane
 *
 */
@Entity
@Table(name = "USER")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserEntity implements IEntity<Integer> {

	/**
	 * L'indentifiant de l'utilisateur.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * Le nom de l'utilisateur.
	 */
	@Column(name = "nom")
	private String nom;

	/**
	 * Le prenom de l'utilisateur.
	 */
	@Column(name = "prenom")
	private String prenom;

	/**
	 * Le numero telephone de l'utilisateur.
	 */
	@Column(name = "telephone")
	private Integer telephone;

	/**
	 * L'email de l'utilisateur.
	 */
	@Column(name = "email")
	private String email;

	@SuppressWarnings("rawtypes")
	@Override
	public IDTOEntity convertToDto() {
		return new User(this);
	}
}
