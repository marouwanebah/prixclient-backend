package com.bah.prixclient.database.entity;

import com.bah.prixclient.referentiel.model.IDTOEntity;

public interface IEntity<Y> {
	
	
	@SuppressWarnings("rawtypes")
	IDTOEntity convertToDto();

}
