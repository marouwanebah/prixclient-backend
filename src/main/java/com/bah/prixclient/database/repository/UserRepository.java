package com.bah.prixclient.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bah.prixclient.database.entity.UserEntity;

/**
 * Le repo des utlisateurs.
 * 
 * @author Marwane
 *
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

}
