package com.bah.prixclient.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bah.prixclient.database.entity.ProduitEntity;

public interface ProduitRepository extends JpaRepository<ProduitEntity, Integer> {

}
